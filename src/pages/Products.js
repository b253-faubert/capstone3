import { useEffect, useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom'
import { Row } from 'react-bootstrap'
import ProductCard from '../components/ProductCard'
import UserContext from '../UserContext';

export default function Products() {
	const { user } = useContext(UserContext)
	const navigate = useNavigate()

	const [products, setProducts] = useState([])

	useEffect(() => {
		if (user.isAdmin) {
			navigate("/admin")
		}
	}, [user])

	useEffect(() => {
		const getProducts = async () => {
			const data = await fetch('http://localhost:4000/products')
			const productsList = await data.json()

			setProducts(productsList.map(product => {
				return (
					
						<ProductCard key={product._id} product={product}/>					
					)
				})
			)
		}
		getProducts().catch(err => console.log(err))
	}, [products])

	return (
		<Row lg={3} md={2}>
			{ products }
		</Row>
		)
}