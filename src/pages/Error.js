import Alert from '../components/Alert'

export default function Error() {

	const data = {
		header: "401 - Unauthorized",
		message: "You are not authorized to view this page",
		destination: "/",
		label: "Back to the home page"
	}

	return (
		<Alert data={data} />
		)
	
}